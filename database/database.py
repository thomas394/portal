from tortoise import Tortoise
from pathlib import Path


basedir = Path.cwd()


async def init():
    await Tortoise.init(
        db_url=f"sqlite://{basedir}/database/db.sqlite",
        modules={'models': ['database.models']}
    )
    await Tortoise.generate_schemas()
