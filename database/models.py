from tortoise.models import Model
from tortoise import fields


class Templates(Model):
    id = fields.UUIDField(pk=True)
    title = fields.CharField(max_length=255)
    url = fields.CharField(max_length=100, null=True)
    doctype = fields.CharField(max_length=8)
    author = fields.CharField(max_length=20)
    created = fields.DatetimeField(auto_now_add=True)
    version = fields.DatetimeField(auto_now=True)
    permission = fields.JSONField(default={})
    required = fields.JSONField(default={})
    article = fields.TextField(null=True)
    active = fields.BooleanField(default=True)
    classification = fields.JSONField(default=[])
    next_doc = fields.UUIDField(null=True)
    prev_doc = fields.UUIDField(null=True)
    doctype = fields.CharField(max_length=20, null=True)


class Projects(Model):
    id = fields.UUIDField(pk=True)
    project = fields.CharField(max_length=12)
    title = fields.CharField(max_length=50)
    type = fields.JSONField(default=[])
    sid = fields.UUIDField(null=True)
    status = fields.CharField(max_length=10)
    options = fields.JSONField(default={})


class Forms(Model):
    id = fields.UUIDField(pk=True)
    project = fields.ForeignKeyField('models.Projects')
    template = fields.ForeignKeyField('models.Templates')
    form = fields.TextField()

