from sys import platform
import uvicorn
from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles
from routers import main_app, checklists
from database.database import init

app = FastAPI(title="TR Panel")
app.mount("/static", StaticFiles(directory="static"), name="static")

# API MODULE
app.include_router(main_app.router)
app.include_router(checklists.router)


# allow Let's Encrypt to access .well-known/acme-challenge directory for domain verification
#  and SSL issuance.
if platform != 'win32':
    app.mount("/.well-known", StaticFiles(directory="static/.well-known"), name="wellknown")
    # db_path = "postgres://panel:sL35P8%e&7pQ@10.240.0.122:5432/panel"
else:
    print("LOCAL")


@app.on_event("startup")
async def init_dbase():
    await init()


if __name__ == "__main__":
    if platform != 'win32':
        uvicorn.run("main:app", host="0.0.0.0", port=443, reload=False,
                    ssl_keyfile="/etc/letsencrypt/live/portal.tebbuttresearch.com/privkey.pem",
                    ssl_certfile="/etc/letsencrypt/live/portal.tebbuttresearch.com/fullchain.pem")
    else:
        uvicorn.run("main:app", host="0.0.0.0", port=80, reload=True)
                    # ssl_keyfile="c:/etc/key.pem",
                    # ssl_certfile="c:/etc/cert.pem")
