from fastapi import APIRouter, Request
from fastapi.templating import Jinja2Templates

from database.models import Projects, Forms, Templates

router = APIRouter()
templates = Jinja2Templates(directory="templates")


@router.get("/compliance/projects")
async def home(request: Request, status: str = 'active'):
    """ Display projects """
    projects = await Projects.filter(status=status).order_by("project")
    project_types = ['Quant', 'Online', 'CATI', 'CAPI', 'Qual', 'FGD', 'DI', 'Recollective', 'Observation']

    return templates.TemplateResponse("compliance_projects.html",
                                      {"request": request, "projects": projects, "status": status,
                                       "project_types": project_types})
