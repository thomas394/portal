from fastapi import APIRouter, Request
from fastapi.templating import Jinja2Templates
from pathlib import Path
from datetime import datetime
import markdown
import parse
from starlette.responses import RedirectResponse
import starlette.status

from wikimarkup.parser import Parser
from database.models import Templates

basedir = Path.cwd()
router = APIRouter()
templates = Jinja2Templates(directory="templates")


@router.get("/")
async def home(request: Request):
    return templates.TemplateResponse("home.html", {"request": request})


@router.get("/exiturl")
async def exit_url(request: Request):
    return templates.TemplateResponse("exit.html", {"request": request})


@router.get("/privacy")
async def exit_url(request: Request):
    return templates.TemplateResponse("privacy.html", {"request": request})


@router.get("/md/{filename}")
async def markdown_test(request: Request, filename: str):
    if filename[-3:] == ".md":
        path = Path.joinpath(basedir, "md", f"{filename}")
    else:
        path = Path.joinpath(basedir, "md", f"{filename}.md")
    with open(path, "r", encoding="utf-8") as input_file:
        text = input_file.read()

    text = text.replace("\\\n* ", "\n\n* ")
    text = text.replace("\n* ", "\n\n* ")
    text = text.replace("\\\n- ", "\n\n- ")
    text = text.replace("\n- ", "\n\n- ")
    text = text.replace("{{", '<span class="training">')
    text = text.replace("}}", '</span>')

    md = markdown.Markdown(extensions=['tables', 'sane_lists', 'meta'])
    html = md.convert(text)

    chk1 = parse.compile("{pre}[ ]{post}")
    chk2 = parse.compile("{pre}[X]{post}")
    br = parse.compile("{pre}\\")
    obsidian = parse.compile("{pre}[[{link}|{text}]]{post}")

    lines = html.splitlines()
    i = 0
    for line in lines:
        if result := chk1.parse(" " + line):
            line = f"{result['pre'][1:]}<input type='checkbox'>{result['post']}"
        if result := chk2.parse(" " + line):
            line = f"{result['pre'][1:]}<input type='checkbox'>{result['post']}"
        if result := br.parse(" " + line):
            line = f"{result['pre']}<br>"
        if result := obsidian.parse(" " + line + " "):
            line = f"{result['pre']}<a href=\"{result['link']}\">{result['text']}</a>"
        lines[i] = line
        i += 1

    return templates.TemplateResponse("raw.html", {"request": request, "html": lines, "meta": md.Meta})


@router.get("/wiki/new")
@router.post("/wiki/new")
@router.get("/wiki/new/{title}")
@router.get("/wiki/edit/{title}")
async def new_markdown(request: Request, title=None):
    j = dict(await request.form())
    if j:
        version = datetime.now().astimezone()
        title = j['title'].strip()
        title = title.replace(" ", "_")
        author = j['author'].strip()
        article = j['article'].strip()
        create = dict(title=title, author=author, version=version, permission=[], required=[], article=article,
                      active=True)
        for key, val in j.items():
            if key in ("chk1", "chk2", "chk3"):
                create['permission'].append(val)

        val = await Templates.get_or_none(title=title)
        if not val:
            val = await Templates.get_or_create(**create)
            print(f"Created article {title}")
        else:
            val = await Templates.filter(title=title).update(**create)
            print(f"Edited article {title}")
        return RedirectResponse(url=f"/wiki/{title}", status_code=starlette.status.HTTP_302_FOUND)
    else:
        article = await Templates.get_or_none(title=title)
        return templates.TemplateResponse("template_new.html", {"request": request, "title": title, "article": article})


def local_link(parser_env, namespace, body):
    (article, pipe, text) = body.partition('|')
    href = article.strip().capitalize().replace(' ', '_')
    text = (text or article).strip()
    return f'<a href="/wiki/{href}">{text}</a>'


@router.get("/wiki/{title}")
@router.get("/wiki")
async def wiki(request: Request, title: str = None):
    chk1 = parse.compile("{pre}{{{{Checkbox|{checkbox}}}}}{post}")
    chk2 = parse.compile("{pre}{{{{Blockquote\n|text={quote}}}}}{post}")
    chk3 = parse.compile("{pre}{{{{training}}}}{post}")
    chk4 = parse.compile("{pre}{{{{induction}}}}{post}")
    chk5 = parse.compile("{pre}- [ ]{line}\n{post}")
    chk5a = parse.compile("{pre}- [x]{line}\n{post}")
    chk6 = parse.compile("{pre}[{title}]({link}){post}")
    chk7 = parse.compile("{pre}![{label}]({url}){post}")
    chk_todo = parse.compile("{pre}{{{{TODO:{todo}}}}}{post}")
    chk_training = parse.compile("{pre}{{{{TRAINING:{training}}}}}{post}")
    chk_note = parse.compile("{pre}{{{{NOTE:{note}}}}}{post}")
    chk_highlight = parse.compile("{pre}{{{{HIGHLIGHT:{note}}}}}{post}")
    chk_pre = parse.compile("{pre}{{{{PRE:{note}}}}}{post}")
    chk_file = parse.compile("{pre}{{{{FILE:{file}|{name}}}}}{post}")

    if not title:
        title = "Compliance"
    title = title.strip()
    article = await Templates.get_or_none(title__iexact=title)
    if not article:
        return await new_markdown(request=request, title=title)

    txt = article.article

    meta = dict(author=article.author, created=article.created, version=article.version, id=article.id)

    parser = Parser()
    parser.registerInternalLinkHook(None, local_link)

    # Convert MD to wiki
    txt = txt.replace("__", "'''")
    txt = txt.replace("\n*", "\n\n*")
    txt = txt.replace("\n\n\n*", "\n\n*")
    txt += "\n "

    # Extra tags - break in bulletpoint list
    txt = txt.replace("\n\\", "<br>")

    chk_id = 0
    while chk := chk5.parse(txt):
        chk_id += 1
        txt = chk['pre'] + "{{Checkbox| |chk_" + str(chk_id) + "|No}}" + chk['line'] + "<br />" + chk['post']

    while chk := chk5a.parse(txt):
        chk_id += 1
        txt = chk['pre'] + "{{Checkbox| |chk_" + str(chk_id) + "|Yes}}" + chk['line'] + "<br />" + chk['post']

    html = parser.parse(txt, show_toc=False)

    # Update HTML for blockquotes
    while chk := chk2.parse(html):
        html = f"{chk['pre']}<blockquote><p>{chk['quote']}</p></blockquote>{chk['post']}"

    # Update training & induction
    while chk := chk3.parse(html):
        html = f"{chk['pre']}<span class='training'>training</span>{chk['post']}"

    while chk := chk4.parse(html):
        html = f"{chk['pre']}<span class='training'>induction</span>{chk['post']}"

    lines = html.splitlines()

    # Update HTML for checkboxes
    i = 0
    for line in lines:
        # Image display
        chk = chk7.findall(" " + line + " ")
        new_txt = ""
        for x in chk:
            md_pre = x['pre']
            md_link = x['url']
            md_caption = x['label']
            md_post = x['post']
            new_txt += f"{md_pre}<img src='{md_link}' alt='{md_caption}' caption='{md_caption}'>{md_post}"
        if new_txt:
            line = new_txt.strip()
            lines[i] = line

        if len(line) > 0:
            if line[-1] == "\\":
                line = f"{line[:-1]}<br />"
                lines[i] = line

        cb = chk1.parse(" " + line + " ")
        if cb:
            pre = cb['pre']
            pre = pre.replace(":", "&emsp;&nbsp;")
            post = cb['post']
            text, cb_id, value = cb['checkbox'].split("|")
            if value.lower() in ('yes', 'true', '1', 'checked'):
                value = " checked='checked'"    # NOTE: the space after first quote is deliberate
            else:
                value = ""
            line = f"{pre}<input type='checkbox' id='{cb_id}'{value}> {text}</input>{post}"
            lines[i] = line

        cb = chk2.parse(line)
        if cb:
            line = f"{cb['pre']}<blockquote><p>{cb['quote']}</p></blockquote>{cb['post']}"
            lines[i] = line

        cb = chk_todo.parse(" " + line + " ")
        if cb:
            line = f"{cb['pre']}<div class='todo'><b>TODO: </b>{cb['todo']}</div>{cb['post']}"
            lines[i] = line

        cb = chk_training.parse(" " + line + " ")
        if cb:
            line = f"{cb['pre']}<div class='training'><b>TRAINING RESOURCE</b><br>{cb['training']}</div>{cb['post']}"
            lines[i] = line

        cb = chk_note.parse(" " + line + " ")
        if cb:
            line = f"{cb['pre']}<div class='note'>{cb['note']}</div>{cb['post']}"
            lines[i] = line

        cb = chk_highlight.parse(" " + line + " ")
        if cb:
            line = f"{cb['pre']}<span class='highlight'>{cb['note']}</span>{cb['post']}"
            lines[i] = line

        cb = chk_pre.parse(" " + line + " ")
        if cb:
            spaces = cb['note'].replace(" ", "&nbsp;")
            line = f"{cb['pre']}{spaces}{cb['post']}"
            lines[i] = line

        cb = chk_file.parse(" " + line + " ")
        if cb:
            file = cb['file']
            display = cb['name']

            line = f'{cb["pre"]}<a href="/static/content/{file}"><span class="icon"><i class="fas fa-download"></i></span>{display}</a>{cb["post"]}'
            lines[i] = line

        i += 1

    return templates.TemplateResponse("raw.html", {"request": request, "html": lines, "meta": meta, "title": title})
