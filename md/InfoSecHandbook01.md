Title: Privacy and Information Security Handbook Section 1
Authors: Tim Wilson
Created: 2022-10-05
Edited: 2022-11-21
Classification: <a href="InfoSecHandbook03">Confidential</a>

# Privacy and Information Security Handbook
[Index](InfoSecHandbook) | [Section 2>>](InfoSecHandbook02.md)

## 1.0 - Confidentiality and Privacy Statement
> *NOTE*: The privacy and confidentiality statement must be the same across all documents / platforms. 
Any change here requires changes to our website, introductions for active projects and the staff handbook.

### Privacy and Confidentiality Statements
__Tebbutt Research Privacy Statement__\
The Tebbutt Research privacy policy is available from the Tebbutt Research website at [tebbuttresearch.com/privacy](https://tebbuttresearch.com/privacy).
It is available to all clients, research participants and suppliers.

For online surveys, it is referred to in invitation emails, reminder emails, and the introduction page.

For telephone surveys, respondents are advised of their rights, advised they can visit our website or call our 
privacy officer for more information, and the policy can be read in full if requested.

The Tebbutt Research’ privacy policy is displayed below:

__Tebbutt Research Confidentiality__\
Confidentiality at Tebbutt Research is considered in the context of data, products, and other information collected and used for the purposes of research.

Privacy at Tebbutt Research is considered in the context of personally identifiable information (PII) that could potentially identify a specific individual.

Tebbutt Research understands the needs and expectations of our research clients, participants, and anyone else who provides information in the form of documents or products, regardless of format or media, and undertakes to treat this information confidentially and secure from misuse.

Where information provided is of a personally identifiable nature, additional processes shall be initiated in accordance with legal and industry standards. 

__Legal and Industry considerations – Australian jurisdiction__\
* The Research Society Code of Professional Behaviour 
* The Research Society fact sheet - MSR and the Privacy Act 1988 and Australian Privacy Principles  
* ISO 20252: 2019 Clause 4.3 Information Security 


__Legal and Industry considerations – International jurisdiction__\
* The ESOMAR Code of Conduct 
* General Data Protection Regulation (GDPR) policy
* Legislation specific to each market we work in

*Refer to [Appendix A](InfoSecHandbookA) for more detail*

__Authorities and responsibilities__\
Responsibilities and authorisations are assigned and included in position descriptions. Information classification is also explained to all staff at {{induction}} and ongoing awareness is maintained via periodic {{training}}.

__General confidentiality and privacy controls__
Assets including buildings and infrastructure, hardware and software, fixed and mobile devices are subject to protective protocols due to the risks associated with data storage.

Unattended electronic assets whether fixed or mobile are secured by the user and monitored centrally by a designated IT officer.  Access controls of devices and software platforms are restricted to designated personnel as administrators.  Individual users do not have access to modify critical system settings.

Secure areas in the building where sensitive documents, products or data are stored are subject to lockable restraints.

Operations security protection protocols are in place to:\
* ensure hardware and software is protected from cyber-attack, malware, and unrestricted use,
* restrict access to equipment on a user-need basis,
* ensure cyber security protocols are maintained,
* operational data is protected via backup from loss of data, and
* operational data is monitored via logs and audits or other monitoring to track technical integrity and risk. 

__Information handling and transfer__\
Information transfer responsibilities, authorisations and protocols are assigned to a senior project manager taking into account client specific requests, legal requirements and local issues related to security of project information on transfer. This applies to all external parties including clients, participants, and outsourced providers/vendors. 

__Your Personal Information__\
In general, the type of personal and sensitive information Tebbutt Research collects and holds includes (but is not limited to): contact details, e.g., name, month, year of birth, postcode and suburb, telephone number, email address, occupation and other details provided by you relating to your family composition, product or service usage as well as a record of your participation in market research activities organised by us. Other details relating to individuals' profiles may include behaviours, attitudes, and opinions. It may also include your bank account details which we may collect for the purposes of distributing incentive payments. (Note, this information is deleted from our system once payment has been successfully made.)

__How we use your personal information__\
Tebbutt Research uses and discloses your personal information for the primary purpose for which it is collected, for reasonably expected secondary purposes which are related to the primary purpose and in other circumstances authorised by the Privacy Act, the GDPR and legislation relevant to your country of residence.

In general, we may use and disclose your personal information for the following purposes:\
* to extend invitations to participate in market research;
* for quality control or validation of research participants; 
* to provide our research clients with profiling information about participants recruited to their study;
* to monitor participation rates;
* to contact past participants to conduct further, follow-up research; 
* to comply with our legal obligations; and 
* to help us manage and enhance our services. 

Apart from the above circumstances, your personal information will not be disclosed without your consent unless legally obliged to do so. 

Personal information about research respondents will only be used in compliance with the Privacy Act, the GDPR, and legislation relevant to your country of residence.

__When we disclose your personal information__\
With your permission, we may disclose your personal information to:\
* other companies or individuals who assist us in providing services or who perform functions on our behalf and organisations that carry out activities essential to our research activities, including for quality control activities, data entry, data processing, conduct of interviewing, contacting people to ask them to participate in research exercises, or 
* anyone else to whom you authorise us to disclose it, usually our client. 

Unless required or authorised by law, the identity of research respondents will not be disclosed to anyone not directly involved with the research project or to (end) clients requesting the research or used for any non-research purpose without the individual's consent. 

__Sending information overseas__\
Data including your personal information is typically collected in your country of residence and may be transferred to our data processing centre in Australia, and our operations centres in Fiji, Papua New Guinea, and Solomon Islands.

All personal information crossing international borders is done so in compliance with the GDPR, including the requirement for data to be encrypted in transit.

__Sensitive information__\
Some personal information which we collect is classified as sensitive information. Sensitive information includes information relating to a person's racial or ethnic origin, political opinions, religion, trade union or other professional or trade association membership, sexual preferences and criminal record, that is also personal information, and health information about an individual. 

Sensitive information will be used and disclosed only for the purpose for which it was provided or a directly related secondary purpose, unless you agree otherwise, or where certain other limited circumstances apply, for example, where required by law.

__Management of personal information__\
The APPs and GDPR require us to take reasonable steps to protect the security of personal information. Tebbutt Research staff are required to respect the confidentiality of personal information and the privacy of individuals. 

Tebbutt Research takes a range of technical, administrative, and physical security steps to protect personal information held from misuse, loss and from unauthorized access, modification, or disclosure, for example by use of physical security and restricted access to electronic records.

Where we no longer require your personal information for a permitted purpose, we will take reasonable steps to de-identify or destroy it.

__How we keep personal information accurate and up to date__\
Tebbutt Research takes reasonable steps to make sure that the personal information it holds is accurate, complete and up to date. Our contact details are set out below. 

__The Tebbutt Research website__\
When visiting our website, the site server makes a record of the visit and logs the following information for statistical and administrative purposes:
* the user’s server address – to consider the users who use the site regularly and tailor the site to their interests and requirements;
* the date and time of the visit to the site – this is important for identifying the website’s busy times and ensuring maintenance on the site is conducted outside these periods;
* pages accessed and documents downloaded – this indicates to Tebbutt Research which pages or documents are most important to users and also helps identify important information that may be difficult to find;
* duration of the visit – this indicates to us how interesting and informative Tebbutt Research’ site is to visitors; 
* the type of browser used – this is important for browser specific coding.

A cookie is a piece of information that an Internet web site sends to your browser when you access information at that site. Cookies are either stored in memory (session cookies) or placed on your hard disk (persistent cookies). The Tebbutt Research website does not use persistent cookies. Upon closing your browser, the session cookie set by this web site is destroyed and no Personal Information is maintained which might identify you should you visit our web site at a later date.

__Retention and destruction of Personal Information__\
Tebbutt Research will destroy or de-identify your personal information as soon as practicable once it is longer needed for the purpose for which it was collected. However, we may be required by law to retain your Personal Information after your relationship with us has expired. In this case your Personal Information will continue to be protected in accordance with this Policy. If we destroy Personal Information, we will do so by taking reasonable steps and using up-to-date techniques and processes.

__Questions, issues, and complaints__\
Where we hold information that you are entitled to access, we will endeavour to provide you with a suitable range of choices as to how access is provided (e.g., through direct login to your profile or by emailing it to you).

If you have any questions about this Privacy Policy or believe that we have at any time failed to keep one of our commitments to you to handle your personal information in the manner required by the Privacy Act, then we ask that you contact us immediately using the following contact details:
* Email our data privacy officer: [[mailto:dpo@tebbuttresearch.com|dpo@tebbuttresearch.com]]
* Telephone our head office in Australia: +61 7 55532 4666

We will respond and advise whether we agree with your complaint or not. If we do not agree, we will provide reasons. If we do agree, we will advise what action we consider is appropriate to take in response. 
If you are still not satisfied after having contacted us and given us a reasonable time to respond, then we suggest that you contact the Office of the Australian Information Commissioner on
Email: [[mailto:enquiries@oaic.gov.au|enquiries@oaic.gov.au]]
