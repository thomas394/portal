Title: Privacy and Information Security Handbook Section 18
Authors: Tim Wilson
Created: 2022-10-05
Edited: 2022-11-21
Classification: <a href="InfoSecHandbook03">Confidential</a>

# Privacy and Information Security Handbook
[<< Section 17](InfoSecHandbook17.md) | [Index](InfoSecHandbook) | [Section 19>>](InfoSecHandbook19.md)

## 18.0 Information security aspects of business continuity policy
>**Objective**\
> Tebbutt Research Information Security continuity shall be embedded in the organization's business 
> continuity management systems and ensure availability of information processing facilities.

Tebbutt Research combines information security aspects of emergency response [ERP], business continuity
[BCP] and disaster recovery [DRP] in an overall single documented planning system. Individual simple 
support documents provide additional information as required.

The Tebbutt Research plan has been developed to react in a timely manner to minimise disruption to 
services in times of crisis. The plans provide minimum standards, assign responsibilities and provide 
direction on what to do should any emergency situation arise.

These events may result in disruption to business activities due to a disabling or otherwise negative 
event such as loss of technology, breach of secure information, infrastructure destruction or critical staffing loss.

### The adverse situation risk and planning process
The information security (IS) continuity plan establishes, documents, implements and maintains:
- IS controls for business continuity or disaster recovery in the case of an emergency
- Maintains existing information security controls during an adverse situation
- Compensates for other standard information security controls that cannot be maintained during an adverse situation

### Review and improvement
Tebbutt Research shall review the validity and effectiveness of information security continuity 
measures when information systems, information security processes and controls and business continuity 
management /disaster recovery management processes and solutions change or otherwise annually.