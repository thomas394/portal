Title: Privacy and Information Security Handbook Section 12
Authors: Tim Wilson
Created: 2022-10-05
Edited: 2022-11-21
Classification: <a href="InfoSecHandbook03">Confidential</a>

# Privacy and Information Security Handbook
[<< Section 11](InfoSecHandbook11.md) | [Index](InfoSecHandbook) | [Section 13>>](InfoSecHandbook13.md)

## 12.0 Physical and environmental policy
> **Objective**
> Prevent unauthorised physical access, damage and interference to the organisation's information and information 
> processing facilities.

The Tebbutt Research physical security and environment policy is developed and implemented to protect the physical 
and infrastructure assets that have the potential to risk data security. This includes access to buildings and 
restricted areas within buildings, fixed and mobile IT equipment including mobile phones, support devices such as 
USB sticks, CDs and other data collection devices, and file server infrastructure.

**Building Security**\
Tebbutt Research' premises is secured using individually signed electronic tags during business hours 
(all entries and exits are logged), plus a deadlock when the office is not occupied. The building has an 
external door that automatically locks outside business hours (6pm-8am) and an elevator that requires an 
electronic tag outside business hours. A register is maintained of all staff that have tags (for the building, 
and for the office).

**USB and CD drives.**\
CDs are not permitted for the purpose of transferring or carrying sensitive data, unless encrypted.

Mass storage read/write is disabled by default on company devices. No USB device can be read unless authorised 
by the CTO. No USB device can be written unless authorised by the CTO and the USB device is hardware encrypted.

**Mobile devices**\
Tebbutt Research recognises the importance of mobile devices as part of employees' roles and supports their 
use to help achieve organisational goals. Mobile devices do represent a risk to information security and data security.

All mobile devices are owned by employees that have access to company email, documents or messaging, 
not including company laptops, but including smartphones and tablets, are subject to this policy.

**Laptop and desktop security and Media Handling**\
Laptops and desktop computers that are used by Tebbutt Research to access sensitive company or client information 
are registered on the Asset Register, including who the device is allocated to (where it has a primary owner). 
When devices are no longer in use, they can be archived within the Asset Register.

When termination of employment occurs, any company laptops shall be returned to the Tebbutt Research General Manager.

**Mobile phones**\
Mobile phones shall be PIN or password protected. Devices are to be maintained and managed as specified under 
this policy.

Any mobile phone used for company purposes may be subject to inspection by the employee's Manager to ensure 
adherence to this policy.

In the event of mobile phone loss (for both company-owned and employee-owned), the employee must report 
the loss immediately to their manager, who will work with the ICT Manager to reset passwords and revoke 
permissions for installed applications like SharePoint, Office 365.