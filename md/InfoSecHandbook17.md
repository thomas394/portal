Title: Privacy and Information Security Handbook Section 17
Authors: Tim Wilson
Created: 2022-10-05
Edited: 2022-11-21
Classification: <a href="InfoSecHandbook03">Confidential</a>

# Privacy and Information Security Handbook
[<< Section 16](InfoSecHandbook16.md) | [Index](InfoSecHandbook) | [Section 19>>](InfoSecHandbook19.md)

## 17.0 Incident security management policy
> **Objective**\
> >Tebbutt Research ensures a consistent and effective approach to the management of information security 
> incidents, including communication on security events and weaknesses.

Tebbutt Research manages information security incidents on the basis of:
- Monitoring to detect suspected incidents as a preventive measure
- Reporting of incidents immediately they are suspected or known
- Undertake an investigation and causal analysis to determine weaknesses within the ISMS
- Manage responses including escalation, controlled recovery, and communication to interested parties
- Reassess Risk Register – is there a new risk?
- Reassess Information Security Policy
- Monitor resolution to ensure incident has been properly resolved

Employees and contractors must report any perceived weaknesses or risk matters to the point of contact ASAP to 
prevent information security events.

The first point of contact of a suspected or known incident is the Project Manager. In the absence of this the 
General Manager or Managing Director shall take control.

As an informed, competent team, the incident will be triaged and appropriate resolution(s) implemented.