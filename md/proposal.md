# Proposal Checklist
## Responding to the request
* [ ] Resolve any uncertainties with the client
* [ ] Resolve any uncertainties in requirements with client
* [ ] Inform client if unable to meet the deadline for proposal
* [ ] Inform client of possible issues (ethical, data protection, etc.)
 
## Contents of the proposal
- [ ] Research objectives (state if proposal does not meet any objectives)
- [ ] Need for translation
- [ ] Methodology
- [ ] Topic areas of QR / DG
- [ ] Analysis approach
- [ ] Analysis scope
- [ ] Sample for quant
    - Target population
	- Sampling procedure
	- Sample frame
	- Extent of representativeness
	- Sample size
	- Subgroup reliability
	- Intended weighting procedures
	- Incidence (if likely to influence cost)
- [ ] Recruitment for qual
	- Recruitment method / source
	- Recency screening method
	- Number of individuals / groups / participants per group
- [ ] Length of interview / FGD / IDI
- [ ] Pilot (Yes / No? How? Quantity?)
- [ ] Quant data
	- Coding
	- Editing
	- Data entry
	- File preparation
- [ ] Qual process
	- Type of venue to be used
	- Availability of viewing facilities
- [ ] Deliverables
	- Coverage, scope, format, delivery method, verbatims, transcripts, recordings, etc.
- [ ] Compliance with professional codes & ISO standard
- [ ] Total cost
- [ ] The currency
- [ ] Tax applicable
- [ ] Term of validity
- [ ] Terms of payment
- [ ] Terms of trade document
- [ ] Execution time of project
- [ ] Specify all elements having an influence on the cost (fieldwork, coding, data entry, analysis, reporting, number of presentations, etc.)
 
## Documentation standards
- [ ] Ensure SharePoint project is set up correctly, with a contact, task list, project file, and enquiry folder
- [ ] Version number in costing document
- [ ] Version number in costing document file name
- [ ] Save client requirements file in enquiry folder (with version number if updated)