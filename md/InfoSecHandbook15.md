Title: Privacy and Information Security Handbook Section 15
Authors: Tim Wilson
Created: 2022-10-05
Edited: 2022-11-21
Classification: <a href="InfoSecHandbook03">Confidential</a>

# Privacy and Information Security Handbook
[<< Section 14](InfoSecHandbook13.md) | [Index](InfoSecHandbook) | [Section 16>>](InfoSecHandbook16.md)

## 15.0 Systems acquisition, development, and maintenance policy
> **Objective**\
> Clients have a level of confidence that Tebbutt Research meets its claimed performance and delivers 
> the necessary information security requirements as required by law.

Tebbutt Research always seeks to acquire software that meets business needs where possible that has proven 
and tested security and privacy compliance.

Tebbutt Research will review potential software vendors and their applications, ensuring they host servers 
in secure data centres, use accepted security measures such as SSL certificates, and, if required, comply 
with any applicable legislation covering security and privacy.