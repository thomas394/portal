Title: Privacy and Information Security Handbook Appendix A
Authors: Tim Wilson
Created: 2022-10-05
Edited: 2022-11-21
Classification: <a href="InfoSecHandbook03">Confidential</a>

# Privacy and Information Security Handbook
[<< Section 19](InfoSecHandbook19.md) | [Index](InfoSecHandbook) | [Appendix B>>](InfoSecHandbookB.md)

## Appendix A – Data and Privacy Legislation across Jurisdictions
_Tebbutt Research will review legislation for each of the jurisdictions we work in at least annually. 
The following websites provide some guidance_

[_https://www.dlapiperdataprotection.com/_](https://www.dlapiperdataprotection.com/)\
[_https://www.dataguidance.com_](https://www.dataguidance.com/)\
[_https://unctad.org/page/data-protection-and-privacy-legislation-worldwide_](https://unctad.org/page/data-protection-and-privacy-legislation-worldwide)\
[_https://iapp.org/resources/global-privacy-directory/_](https://iapp.org/resources/global-privacy-directory/)

_Tebbutt Research will adhere to the GDPR and APP as a minimum standard across all jurisdictions where these 
are not in conflict with local laws._

**AMERICAN SAMOA**\
American Samoa is an unincorporated territory of the United States. The American Samoan government invokes the 
US Privacy Act of 1974 (e.g. [https://www.legalaffairs.as.gov/legal-notice](https://www.legalaffairs.as.gov/legal-notice))

Tebbutt Research will protect PII in a way that is consistent with the principles of the Privacy Act of 
1974 (5 U.S.C. § 552a) when conducting work in American Samoa.

The Privacy Act guarantees three primary rights:

- The right to see records about oneself, subject to Privacy Act exemptions;
- The right to request the amendment of records that are not accurate, relevant, timely or complete; and
- The right of individuals to be protected against unwarranted invasion of their privacy resulting from the 
    collection, maintenance, use, and disclosure of personal information.

**COOK ISLANDS**\
No specific data protection legislation has been adopted in Cook Islands 
([https://www.dataguidance.com/jurisdiction/cook-islands](https://www.dataguidance.com/jurisdiction/cook-islands))

**FSM**\
There appears to be no specific legislation in FSM for data protection or privacy, however the country is 
closely aligned with the United States and we will adopt the US Privacy Act of 1974 as a minimum standard 
when conducting work in these states.

According to [https://unctad.org/page/data-protection-and-privacy-legislation-worldwide](https://unctad.org/page/data-protection-and-privacy-legislation-worldwide) there is no legislation for Privacy and Data Protection in FSM.

**FIJI**\
The IAPP is unaware of specific data protection legislation in Fiji.\
[_https://www.dataguidance.com/jurisdiction/fiji_](https://www.dataguidance.com/jurisdiction/fiji)

Whilst there is no general data protection law in Fiji, the Constitution of the Republic of Fiji (2013) 
provides for a right to privacy, which includes a right to confidentiality of personal information. 
In addition, there are sectoral laws regulating electronic transactions, cybercrime, and consumer protection. 
In particular, obligations for telecommunication service providers regarding customer data confidentiality, 
and consent requirements are outlined under the Telecommunications Promulgations 2008. Additionally, the 
Online Safety Act, 2018 came into effect in January 2019, which aims to, among other things, deter misuse of 
personal information online. The Cybercrime Act 2021 was also enacted by the Parliament of the Republic of Fiji.\
[_https://www.dlapiperdataprotection.com/_](https://www.dlapiperdataprotection.com/)

There is no specific legislation for personal data protection in Fiji. Clause 24 of the Constitution (2013) 
provides the right to personal privacy, includes right to confidentiality of personal information.

Some sector-specific laws criminalise (or expose to other serious action) the unauthorised disclosure by 
others of personal/client information as follows:

- Banking Act 1995 – by central bank personnel (s.27) and licensed financial institution personnel (s.71)
- Fiji Revenue and Customs Service Act 1998 – by tax officials (s.52 (2))
- Medical and Dental Practitioner Act 2010 – by statutory administrators of any data obtained in the course 
    of their duties (s.126)
- Under the Rules of Professional Conduct and Practice (para 1.4) of the Legal Practitioners Act 2009 – information 
    received by legal practitioners from or on behalf of clients
- Cybercrime Act 2021 (has not commenced yet) - Defines 'computer data' which is broad enough to capture 
    personal data if it stored in a computer system.

These laws, however, do not directly protect personal information.

**Definition of Personal Data**\
The only actionable rights available to citizens are in s.24 of the Constitution. This creates a right to 
"personal privacy", said to include:
- "confidentiality of their personal information;
- confidentiality of their communications; and
- respect for their private and family life".

These terms are not otherwise defined.

**FRENCH POLYNESIA**\
French Polynesia is a French overseas territory. 
[The CNIL has guidance](https://www.cnil.fr/fr/loi-informatique-et-libertes-et-rgpd-ce-qui-change-pour-loutre-mer) 
regarding the applicability of the GDPR and the French data protection act, Informatique et Libertés.

Tebbutt Research will apply the GDPR as a minimum standard when working in French Polynesia.

**GUAM**\
There is no general privacy law in effect. Guam has not enacted comprehensive data privacy legislation. 
However, like other states and territories in the US it has enacted a data breach law 
through [§48-10 of Chapter 48 of Title 9 of the Guam Code Annotated](https://platform.dataguidance.com/legal-research/notification-breaches-personal-information-%C2%A748-10-chapter-48-title-9-guam-code) 
('the Data Breach Law'). Notably, the Data Breach Law only apples to electronic data and is not applicable 
to encrypted or redacted data. Furthermore, good-faith acquisitions of personal information by employees 
or agents are also excluded and notification is only required to individuals. Moreover, individuals and entities 
may be liable for civil penalties for violation not exceeding $150,000 per breach or series of breaches uncovered 
in a single investigation. There are currently no pending data protection laws in the Guam legislature and, 
as such, privacy protections are largely provided through US federal privacy laws.

Definition of PII: First name, or first initial, and last name in combination with and linked to any one or 
more of the following data elements that are neither encrypted nor redacted:
- Social Security Number;
- Driver's license number or Guam identification card number issued in lieu of a driver's license;
- Financial account number, or credit card or debit card number, in combination with any required security code, 
    access code, or password that would permit access to a resident's financial accounts.

The term does not include information that is lawfully obtained from publicly available information, or from 
Federal, State, or local government records lawfully made available to the general public.

Definition a Breach: Unauthorized access and acquisition of unencrypted and unredacted computerized data that 
compromises the security or confidentiality of personal information maintained by an entity as part of a 
database of personal information regarding multiple individuals, or the individual or entity reasonably 
believes has caused or will cause identity theft or other fraud. Excludes certain good faith acquisitions.

Encryption Safe Harbour: Statute does not apply to encrypted or redacted personal information. The safe harbor 
does not apply when the encryption key was also compromised due to breach or there is a reasonable belief 
that a resident will suffer identity theft or fraud.

Risk of Harm Analysis: Notification is not required if the acquisition of personal information does not cause, 
or the subject entity does not reasonably believe it has or will cause, identity theft or other fraud 
to a Guam resident.

**KIRIBATI**\
According to [https://unctad.org/page/data-protection-and-privacy-legislation-worldwide](https://unctad.org/page/data-protection-and-privacy-legislation-worldwide) 
there is no Privacy and Data Protection legislation in Kiribati.

There appears to be no specific privacy or data protection legislation in Kiribati, however the 
Ministry of Information, Communications and Transport cites their website is compliant with "UK and European law". 
There is reference to both the UK Data Protection Act (DPA 2018) and the GDPR.\
(e.g. [https://www.getsafeonline.org.ki/kiribati/privacy-policy/](https://www.getsafeonline.org.ki/kiribati/privacy-policy/))

Tebbutt Research will adhere to the GDPR principles while working in Kiribati.

**MARSHALL ISLANDS**\
[_https://www.dataguidance.com/jurisdiction/marshall-islands_](https://www.dataguidance.com/jurisdiction/marshall-islands)

Law: There is no general personal data protection law.\
Regulator: There is no general data protection authority.\
Summary: While the Republic of Marshall Islands does not have a personal data protection law, nor any 
legislation governing cybersecurity, the Marshall Islands has participated in international discussions 
related to cybersecurity and is a member of the Pacific Cyber Security Operational Network which, 
among other things, aims to strengthen cybersecurity across the across the Pacific. 
In addition, on 26 February 2018, the 
[Parliament of the Republic of Marshall Islands](https://rmiparliament.org/cms/) 
('Nitijela') passed the [Declaration and Issuance of the Sovereign Currency Act 2018](https://sov.foundation/law.pdf), 
which set the ground for the issuance of a digital decentralised currency based on blockchain technology, 
the Sovereign ('SOV'), as a legal tender. The SOV is set to be the first legal tender digital currency 
in the world, and a system of accredited verifiers and cryptographically signed SOV IDs are expected to be 
employed to protect user privacy.

According to [https://unctad.org/page/data-protection-and-privacy-legislation-worldwide](https://unctad.org/page/data-protection-and-privacy-legislation-worldwide) 
there is no Privacy and Data Protection legislation in Marshall Islands.

**NAURU**\
According to 
[https://unctad.org/page/data-protection-and-privacy-legislation-worldwide](https://unctad.org/page/data-protection-and-privacy-legislation-worldwide) 
there is no Privacy and Data Protection legislation in Nauru.

It appears Nauru has no specific data protection or privacy legislation. Nauran law is historically in close 
alignment with that of Australia and the UK. We will adopt the Australian Privacy Principles as a minimum standard 
when working in Nauru.

**NEW CALEDONIA**\
New Caledonia is a French overseas territory. 
[The CNIL has guidance](https://www.cnil.fr/fr/loi-informatique-et-libertes-et-rgpd-ce-qui-change-pour-loutre-mer) 
regarding the applicability of the GDPR and the French data protection act, Informatique et Libertés.

Tebbutt Research will apply the GDPR as a minimum standard when working in New Caledonia.

**NIUE**\
No data found to date. Niue is a protectorate of New Zealand, and we will consider NZ law as a minimum standard 
when working in Niue.

**PAPUA NEW GUINEA**\
The IAPP is unaware of specific data protection legislation in Papua New Guinea.

According to 
[https://unctad.org/page/data-protection-and-privacy-legislation-worldwide](https://unctad.org/page/data-protection-and-privacy-legislation-worldwide) 
there is no Privacy and Data Protection legislation in PNG.

**PALAU**\
According to 
[https://unctad.org/page/data-protection-and-privacy-legislation-worldwide](https://unctad.org/page/data-protection-and-privacy-legislation-worldwide) 
there is no Privacy and Data Protection legislation in Palau.

**SAMOA**\
According to 
[https://unctad.org/page/data-protection-and-privacy-legislation-worldwide](https://unctad.org/page/data-protection-and-privacy-legislation-worldwide) 
there is no Privacy and Data Protection legislation in Samoa.

**SOLOMON ISLANDS**\
The IAPP is unaware of data protection legislation in the Solomon Islands.

According to 
[https://unctad.org/page/data-protection-and-privacy-legislation-worldwide](https://unctad.org/page/data-protection-and-privacy-legislation-worldwide) 
there is no Privacy and Data Protection legislation in Solomon Islands.

**TIMOR-LESTE**\
According to 
[https://unctad.org/page/data-protection-and-privacy-legislation-worldwide](https://unctad.org/page/data-protection-and-privacy-legislation-worldwide) 
there is no Privacy and Data Protection legislation in Timor-Leste.

Whilst there is no general data protection law in Timor-Leste, the 
[Constitution of the Democratic Republic of Timor-Leste](https://www.dataguidance.com/legal-research/constitution-democratic-republic-timor-leste), 
which was enacted in 2002, provides every individual with the right to privacy. Furthermore, Article 38 
of the Constitution specifically addresses personal data protection and provides every citizen with the 
right to access, the right to rectification, and the right to know the purpose for which their personal 
data was collected, as well as outlines consent requirements for the processing of certain types of data. 
In addition, there are sectoral laws regulating money laundering and the national health system.

**TOKELAU**\
No data found to date. Tokelau is a protectorate of New Zealand, and we will consider NZ law as a minimum 
standard when working in Tokelau.

**TONGA**\
According to 
[https://unctad.org/page/data-protection-and-privacy-legislation-worldwide](https://unctad.org/page/data-protection-and-privacy-legislation-worldwide) 
there is no Privacy and Data Protection legislation in Tonga.

**TUVALU**\
According to 
[https://unctad.org/page/data-protection-and-privacy-legislation-worldwide](https://unctad.org/page/data-protection-and-privacy-legislation-worldwide) 
there is no Privacy and Data Protection legislation in Tuvalu.

**VANUATU**\
According to 
[https://unctad.org/page/data-protection-and-privacy-legislation-worldwide](https://unctad.org/page/data-protection-and-privacy-legislation-worldwide) 
there is no Privacy and Data Protection legislation in Vanuatu.

**WALLIS & FUTUNA**\
Wallis & Futuna is a French overseas territory. 
[The CNIL has guidance](https://www.cnil.fr/fr/loi-informatique-et-libertes-et-rgpd-ce-qui-change-pour-loutre-mer) 
regarding the applicability of the GDPR and the French data protection act, Informatique et Libertés.

Tebbutt Research will apply the GDPR as a minimum standard when working in Wallis & Futuna.