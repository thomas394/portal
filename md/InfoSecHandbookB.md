Title: Privacy and Information Security Handbook Appendix B
Authors: Tim Wilson
Created: 2022-10-05
Edited: 2022-11-21
Classification: <a href="InfoSecHandbook03">Confidential</a>

# Privacy and Information Security Handbook
[<< Appendix A](InfoSecHandbookA.md) | [Index](InfoSecHandbook)

## Apendix B – Australian Privacy Principles

A full guideline to the Australian Privacy Principles (APP) can be found on the OAIC website:\
[https://www.oaic.gov.au/privacy/australian-privacy-principles/read-the-australian-privacy-principles](https://www.oaic.gov.au/privacy/australian-privacy-principles/read-the-australian-privacy-principles)

This appendix serves as a guide to the sections of the code that are most relevant to our business.

To meet the requirements of the APP, we must…\
- Be open and transparent:
    - What type of PII is collected and held
    - How it is collected and held
    - The purpose of collection
    - How PII can be accessed and corrected
    - How complaints can be raised
    - Whether there are overseas recipients, and what countries are involved
- Collect only PII that is reasonably necessary
- Gain informed consent
- Not disclose PII for secondary purposes without consent
- Ensure overseas recipients will protect the data to the same extent as the APP
- Use our own identifiers for individuals (not a government issued identifier)
- Maintain the integrity of PII and correct data when requested
- Protect the security of the data (from misuse, loss, unauthorized access, etc)