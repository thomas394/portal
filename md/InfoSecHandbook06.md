Title: Privacy and Information Security Handbook Section 6
Authors: Tim Wilson
Created: 2022-10-05
Edited: 2022-11-21
Classification: <a href="InfoSecHandbook03">Confidential</a>

# Privacy and Information Security Handbook
[<< Section 5](InfoSecHandbook05.md) | [Index](InfoSecHandbook) | [Section 7>>](InfoSecHandbook07.md)

## 6.0 Information Security Policy

>**Objectives**\
The following Information Security Management System (ISMS) objectives apply to all aspects of the ISMS and are measured for performance and improvement through executive review meetings. \
> [ ] Confidentiality\
> [ ] Integrity\
> [ ] Accessibility

Tebbutt Research is committed to provide, manage, maintain and continuously improve an information security management system [ISMS] that enables:

- Tebbutt Research to meet client needs and expectations
- Confidentiality protection provisions of information held by Tebbutt Research
- Integrity of the systems structure affording Tebbutt Research protection from unauthorised access or intrusion
- Systems to ensure the availability of data and protection from loss or corruption

The ISMS is enabled through a series of policies, second tier objectives against each policy and documented systems and technology solutions.