Title: Privacy and Information Security Handbook Section 3
Authors: Tim Wilson
Created: 2022-10-05
Edited: 2022-11-21
Classification: <a href="InfoSecHandbook03">Confidential</a>

# Privacy and Information Security Handbook
[<< Section 2](InfoSecHandbook02.md) | [Index](InfoSecHandbook) | [Section 4>>](InfoSecHandbook04.md)

## 3.0 Information Classification Policy

> **Objective**\
> The Tebbutt Research information classification framework and controls ensure that, when necessary, some 
> information access is restricted to meet privacy and legislative requirements. Other information however is shared 
> to enable information use within Tebbutt Research to deliver its services.

### Information Classification Policy

Tebbutt Research has a responsibility to protect the information that it creates and collects. 
The level of protection provided depends on the nature of the information collected.

Access to information is provided to staff and third parties to allow them to undertake their day-to-day activities. 
It is recognised that there is an underlying need to facilitate ongoing sharing of information where appropriate. 
Any information created becomes a record and is managed according to its risk classification. 
A risk assessment process shall determine the risk hierarchy considering the following:

**Confidentiality** ensuring that only the required persons have access to information and preventing unauthorised 
access or disclosure.

Confidentiality defines the requirements for protecting information against unauthorised access or disclosure 
that may negatively impact on Tebbutt Research and its stakeholders.

**Integrity** ensuring accuracy and completeness of information and only authorised persons can modify information.

Integrity defines the requirements for protecting information against unauthorised modification that may 
negatively impact Tebbutt Research and its stakeholders. Integrity to also seek to ensure the information is correct, 
accurate and traceable.

**Availability** ensuring the information is available when and where it is required.

Availability defines the requirements for effective controls that ensure critical information is available as soon 
as it is needed to those with authorised access.

**Retention** ensuring information is archived and stored for an acceptable period to meet legislative and 
compliance requirements.

Information shall be retained or archived for a period of time to ensure legislative and contractual requirements 
are met. Retention of electronic information requires planning to ensure the information is archived in a format 
that will maintain accessibility for the required period of retention and is then securely destroyed in a timely manner.

### Information Classifications

Information will be classified into 4 categories:
- Public – Document can be distributed freely. No classification placed on document.
- Commercial in Confidence – Used for documents such as proposals, between Tebbutt Research and clients. 
  These documents can be accessed by Project/ Operations Managers but not by general Field Workers.
- Confidential – Only for Tebbutt Research staff, and only if their role grants them access to view
- Highly Confidential – Used for documents that contain personal information, e.g., HR related. 
  These documents are available to the senior management team only (Managing Director and General Managers).

Any restricted document (non-Public classification) will have its classification placed in the footer section of 
each page. Restricted documents are not to be left on desks in plain view when unattended.
