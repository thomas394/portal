Title: Privacy and Information Security Handbook Section 5
Authors: Tim Wilson
Created: 2022-10-05
Edited: 2022-11-21
Classification: <a href="InfoSecHandbook03">Confidential</a>

# Privacy and Information Security Handbook
[<< Section 4](InfoSecHandbook04.md) | [Index](InfoSecHandbook) | [Section 6>>](InfoSecHandbook06.md)

# 5.0 Data Breach Response Plan

## Data Breach Response Plan

### Breach Reporting Procedures for Employees

The Tebbutt Research Managing Director, General Managers, Data Privacy Officer, Operations Managers, and employees 
are committed to protecting the privacy of clients and participants.

This data breach response plan outlines definitions, sets out procedures and clear lines of authority for 
Tebbutt Research staff in the event that Tebbutt Research experiences a data breach, or suspects that a data breach 
has occurred.

This Plan operates under our Privacy Policy and must be followed when assessing and responding to an actual or 
suspected data breach.

### Key Concepts

#### Personal information

Personal information means information about an identified individual or an individual who is reasonably identifiable, 
whether the information is true or not and whether the information is recorded in a material form or not.

It includes all personal information regardless of its source and regardless of whether it is publicly available.

#### Data Breach

A data breach occurs when personal information is lost or subjected to unauthorised access, modification, use or 
disclosure or other misuse.

Data breaches may include (but are not limited to) unauthorised access by a third party, information accidentally 
being uploaded to a public website or a laptop or USB drive containing personal information being lost or stolen and can be caused by or exacerbated by a variety of factors, effect different types of personal information and give rise to a range of actual or potential harms to individuals, agencies, or organisations.

#### Eligible Data Breach

An eligible data breach is a data breach that is likely to result in serious harm to any of the individuals to whom 
the information relates.

#### Notifiable Data Breach

Not all data breaches require notification. The Notifiable Data Breaches (NDB) scheme only requires organisations 
to notify when there is a data breach that is likely to result in serious harm to any individual to whom the 
information relates.

The Tebbutt Research must notify the Office of the Australian Information Commissioner (OAIC) and **affected 
individuals if** :

- It has grounds to believe that an eligible data breach has occurred; or
- It is directed to do so by the OAIC (for instance if a data breach is reported directly to the OAIC by an 
  affected individual and/or if the OAIC disagreed with the Tebbutt Research' assessment that the incident is 
  not an eligible data breach).

## TEBBUTT RESEARCH DATA RESPONSE TEAM

### Key Roles and Responsibilities

The Data Breach Response Team for the Tebbutt Research is made up of the Managing Director of Tebbutt Research, the Data Privacy Officer, the External ICT Consultant, and the Programmer and/or Database Manager if applicable.

| **Title** | **Role** |
| --- | --- |
| Tebbutt Research employees(within an hour of realizing breach may have occurred) |- Report incidents immediately to the Managing Director and Data Data Privacy Officer- Participate in investigation as required|
| Managing Director/ Data Data Privacy Officer(ideally within first 12 hours) |- Receive information on the incident<br>- Contain breach, remediate harm and preserve evidence<br>- Conduct an investigation<br>- Complete data breach report |
| Data Breach Response Team (ideally within 24 hours) |- Review investigation findings<br>- Accept or reject findings<br>- Determine seriousness of the data breach<br>- Assess containment and/or remediation actions<br>- Assess preliminary investigation<br>- Assess whether an eligible data breach has occurred<br>- Assess notification requirements<br>- Assist with post-action review |

### Timeframes

An actual or suspected data breach must be investigated and managed as soon as the Tebbutt Research becomes aware of the data breach or suspects that it has occurred.

Immediate action should be conducted on any breach and affected individuals (if breach confirmed) should be notified ASAP (ideally within 24 hours)

| **Suspected data breach** | **Confirmed eligible data breach**|
| --- | --- |
| Assessments must be reasonable and expeditious (ideally within 24 hours).All reasonable steps to complete the assessment within **30 days** of the date that the Tebbutt Research became aware or suspected a data breach.This timeframe should be treated as the maximum timeframe for completing the assessment. | Best practice is to notify affected individuals ASAP (ideally within 24 hours) Notify the OAIC and affected individuals **as soon as practicable** after becoming aware of an eligible data breach. |

### Assessing Suspected Data Breaches

If any Tebbutt Research employee suspects or becomes aware of a data breach, this plan is activated and must be followed. The plan requires a reasonable and expeditious assessment to determine if the data breach is likely to result in serious harm.

![data_breach_procedure](/static/content/data_breach_procedure.png)

### Data Breach Response Process:

There are four key steps to consider when responding to a breach or suspected data breach.

- PHASE 1: Contain the breach and do a preliminary assessment
- PHASE 2: Evaluate the risks associated with the breach
- PHASE 3: Notification
- PHASE 4: Prevent future breaches

#### PHASE 1: REPORT & CONTAIN
**1.1 Procedure for Reporting Data Breaches**
If any Tebbutt Research employees become aware of an actual or suspected data breach, they must report it as soon as possible and do the following immediately:

Record the details of the data breach in the **Data Breach Response Information Report** {{(found in SharePoint)}}

- Provide copy of the form to the Managing Director in person or by email. If the Managing Director is on leave, then to the Data Privacy Officer.
- Keep the incident confidential except where it is necessary to disclose information in accordance with this Plan.

Upon receiving the Data Breach Response Information Report form, the Managing Director/ Data Privacy Officer must immediately:

- Take action to contain the breach, remediate harm and preserve evidence.

**1.2 Procedure for Containing Data and Remediating Harm**

The Managing Director is responsible for taking immediate action to contain the breach and remediate harm, including by seeking assistance from the Data Privacy Officer if required.

**1.3 Preserving Evidence of a suspected data breach**

The Managing Director must take any reasonable steps available to them to preserve and/or record evidence of an actual or suspected data breach **.**

**DATA BREACH - MANAGEMENT PROCEDURES**

- PHASE 2: Investigate and Evaluate the risks associated with the breach
- PHASE 3: Notification
- PHASE 4: Prevent future breaches/ Review

#### PHASE 2: INVESTIGATE & EVALUATE

**2.1 Procedure for investigating reported data breaches:**

The Managing Director must review any report of an actual or suspected data breach as soon as reasonably practicable. Upon reviewing the Data Breach Form, The Managing Director should:

- Assess the containment and/or remediation actions and whether any further actions are required; and
- Undertake any preliminary investigations necessary to confirm the report and/ or seek clarification or additional detail as necessary.

**2.2 Reporting and Escalation**

The Managing Director should discuss the findings of the preliminary investigation to the Data Privacy Officer as soon as possible and use discretion in deciding whether to escalate to the Data Breach Response Team.

**2.3 Procedure for escalation to the Data Breach Response Group**

In determining whether to escalate data breaches to the Data Breach Response Team, the Managing Director should consider the following questions:

- Are multiple individuals affected by the breach or suspected breach?
- Is there (or may there be) a real risk of serious harm to the affected individual(s)?
- Does the breach or suspected breach indicate a systemic problem in Tebbutt Research' processes or procedures?
- Could there be media or stakeholder attention because of the breach or suspected breach?

**IF NO - if the Managing Director decides not to escalate a minor data breach or suspected data breach to the Data Breach**

**Response Team for further action, the Managing Director should:**

- send a brief email to the Data Response Team that contains the following information:
    - description of the breach or suspected breach
    - action taken by the Managing Director or Data Privacy Officer to address the breach or suspected breach
    - the outcome of that action, and
    - the Managing Director's view that no further action is required
- save a copy of that email in SharePoint:

**IF YES - if the Managing Director determines that the answer to any of these questions is 'yes', then it may be appropriate for the Managing Director to notify the Data Breach Response Team.**

**2.4 Members of the Data Breach Response Group:**

The Data Breach Response Group comprises the following permanent members:

- Managing Director
- Data Privacy Officer
- External IT Consultant

The Managing Director may co-opt additional members onto the Data Breach Response Group or engage external providers to assist in containment or investigation of the breach, depending on the nature or severity of the data breach.

**2.5 Evaluating a serious risk of harm to an individual by the Data Response Team**

In evaluating whether there is a serious risk of harm to an individual whose information is the subject of a data breach, the Data Response Team must consider:

- what type of personal information is involved (and in particular, whether it is sensitive information);
- whether there are any protections that would prevent the party who receives (or may have received) the personal information from using it (for example, if it is encrypted);
- the nature of the harm that could arise from the breach, for example whether an individual was reasonably likely to suffer:
  - identity theft;
  - financial loss;
  - a threat to their physical safety;
  - a threat to their emotional wellbeing;
  - loss of business or employment opportunities;
  - humiliation, damage to reputation or relationships; or
  - workplace or social bullying or marginalisation;
- what steps have been taken to remedy the breach (and how certain The Tebbutt Research is that they are effective).

#### PHASE 3: NOTIFICATION

**3.1 Notifying the Office of the Australian Information Commissioner (OAIC)**

In the event that the Data Breach Response Team decides there has been a data breach and there is a real risk of serious harm to affected individuals the Data Breach Response Team must prepare a statement that includes:

- Tebbutt Research' contact details;
- a description of the data breach;
- the kind of information concerned; and
- Recommendations about the steps that individuals should take in response to the eligible data breach that the entity has reasonable grounds to believe has happened.

The statement must be submitted to OAIC via email to [enquiries@oaic.gov.au](mailto:enquiries@oaic.gov.au) as soon as reasonably practical.

Complete the OAIC Notification Template {{(template in SharePoint)}}

**3.2 Notifying the individuals affected**

As soon as reasonably practical after Tebbutt Research has submitted the statement to the OAIC, Tebbutt Research must:

- if practical, take reasonable steps to notify the contents of the statement to each of the individuals to whom the information relates; or
- if practical, take reasonable steps to notify contents of the statement to each of the individuals who are at risk from the eligible data breach.

If it is not practical to undertake either of the above, the Data Breach Response Team must ensure a copy of the statement is published on the Tebbutt Research' website and reasonable steps are taken to publicise the contents of the statement (for example, by notifying its members).

**3.3 Record keeping**

Documents created by the Data Breach Response Team should be saved in {{SharePoint}}.

#### PHASE 4: PREVENT FUTURE BREACHES & REVIEW

**4.1 Procedure for Conducting Post Breach Review**

The Managing Director and/ or Data Privacy Officer is responsible for conducting a post-breach review and assessment, once the immediate consequences of the data breach have been dealt with.

## 5.1 Notifiable Data Breach Template

The OAIC's Notifiable Data Breach Form is available at 
[https://forms.uat.business.gov.au/smartforms/servlet/SmartForm.html?formCode=OAIC-NDB](https://forms.uat.business.gov.au/smartforms/servlet/SmartForm.html?formCode=OAIC-NDB)

The OAIC form requires the following information:\
**Organisation name:** Tebbutt Research\
**Phone:** Ph: +61 7 55532 4666\
**Email:** dpo@tebbuttresearch.com\
**Address:** Suite 30505, 9 Lawson St, Southport, QLD, 4209\
Description of the eligible data breach:\
Information involved in the data breach (kinds of personal information):\
In addition, please select any categories that apply:\
- [ ] Financial details
- [ ] Tax File Number (TFN)
- [ ] Identity information (e.g. Centrelink reference number, passport number, driver license number)
- [ ] Contact information (e.g. home address, phone number, email address)
- [ ] Health information
- [ ] Other sensitive information (e.g. sexual orientation, political or religious views)

Recommended steps – to individuals to reduce harm. We advised the people affected to do the following:\
Other entities affected

Was another organisation affected?\
Contact details:\
Title & Name:\
Phone:\
Email:\
Date breach occurred:\
Date breach was discovered:\
Primary cause of breach :\
Exact number of individuals whose personal information was involved in the data breach:\
Description of how the data breach occurred:\
Description of any action, including remedial action taken to people whose personal information was involved in the data breach.\
Description of any action taken or are intending to take to prevent reoccurrence.\
How do you intend to notify individuals who are likely to be at risk of serious harm as a result of the data breach?\
List any other data protection authorities, law enforcement bodies that you have reported this to.\
Please attach a template copy of your notification to affected individuals.