Title: Privacy and Information Security Handbook Section 8
Authors: Tim Wilson
Created: 2022-10-05
Edited: 2022-11-21
Classification: <a href="InfoSecHandbook03">Confidential</a>

# Privacy and Information Security Handbook
[<< Section 7](InfoSecHandbook07.md) | [Index](InfoSecHandbook) | [Section 9>>](InfoSecHandbook09.md)

## 8.0 Human resources security policy
>**Objective**\
Tebbutt Research shall manage human resources whether employed or contracted, in a manner that does not compromise 
> the Tebbutt Research security or client data.

Tebbutt Research employs staff and engages some contracted services according to business needs.

As a minimum the following minimum standards apply:

**Permanent Employees** – All permanent appointments are made by the Managing Director and General Manager only

- New employees shall be subject to business and/or character references, identity validation and work 
    availability status (visa) where information security access is considered a business risk;
- Employment agreement;
- Annual performance review;
- Periodic awareness and refresher training;
- Exit process at termination of employment that secures the integrity of Tebbutt Research data and intellectual 
    property as applicable to the role.

**Casual / Part-time Employees** – All casual or part-time employee appointments are made by an operations manager or higher.

- New employees are subject to identity verification and work status;
- Employment agreement;
- Annual performance review;
- Periodic awareness and refresher training;
- Exit process at termination of employment that secures the integrity of Tebbutt Research data and intellectual 
    property as applicable to the role.

**Contracted services** – All appointments made by Managing Director and General Manager only

- Review and preapproval process for new contracted organisations subject to approval by the Managing Director 
    and/or General Manager only – based on capabilities and information security protocols;
- Quarterly performance review of work quality and productivity by the Managing Director and/or General Manager;
- Exit process at termination of contract that secures the integrity of Tebbutt Research and client data and 
    intellectual property as applicable to the role.

## 8.1 Disciplinary action policy
> **Objective**\
> Where there is a need for disciplinary action, a formal process in line with legislation shall be followed 
> with the aim of providing a fair and equitable outcome.


Tebbutt Research employees are expected to conduct themselves in accordance with:

- documented work practices,
- accepted standards of behaviour and respect of others,
- be responsible for the care of Tebbutt Research property under their control, and
- abide by the laws of each country we work in, including employment and work acts, occupational health and 
    safety legislation, and information security as it applies in all working jurisdictions.

Allegations of a breach of legislation whether intended or unintended, misconduct or breach of any company policy 
will be considered as unsatisfactory performance and a disciplinary process may be initiated.

Should a formal warning or termination of employment be a necessary outcome of a disciplinary process, an 
investigation shall occur, and the outcome of the investigation will dictate what, if any, action is appropriate.

## 8.2 Social media protocols

> **Objective**\
> Tebbutt Research encourage social media use but as an employee of this organisation and as an individual there 
> is a need to make sure you are aware of the risks, particularly those that threaten Tebbutt Research' reputation 
> and that of your own.

When accessing the Tebbutt Research internal social media networks, staff must use Tebbutt Research information 
communications technology (ICT) facilities in an acceptable manner according to this policy. Social media use 
should not interfere with work performance and productivity.

Tebbutt Research will not tolerate illegal and aggressive online behaviour in any form.

When using social media at work, staff are expected to:
- be polite and respectful of the opinions of others at all times;
- be mindful that others may not share the same sense of humour;
- not use Tebbutt Research ICT resources to provide comments to any news media, politicians or lobby groups – always 
    remain apolitical.
- not access or engage with any material that is inappropriate or unlawful. This may include posts that are 
    fraudulent, threatening, bullying, embarrassing, of a sexual nature, obscene, racist, sexist, defamatory or 
    profane, whether obscured by symbols or not;
- not use Tebbutt Research ICT resources to post explicit or sexually suggestive messages;
- not infringe another person, or Tebbutt Research', intellectual property rights.

In a fast-changing world good judgment in social media interaction needs to be exercised. The following standards 
and protocols apply without exception.

**Truth and facts**\
- Never misrepresent the facts in chat rooms and other online discussion forums;
- Always quote the source of the fact;
- Seek confirmation of facts before publishing on social media;
- If it is your opinion and not a fact, then say so and attribute to yourself not to another person or Tebbutt Research.

You may be legally responsible for the content of your post, so respect brands, trademarks and copyrights. 
Terms and conditions always assign a level of responsibility to the user. Before joining social media sites, 
read and ensure you are prepared to follow the terms and conditions of the site.

**Disclosure and affiliations**\
If you talk online about work related matters that are within your area of job responsibility you must disclose 
your affiliation with Tebbutt Research.

On personal social media sites (not Tebbutt Research) unless you are specifically authorised to speak on behalf 
of Tebbutt Research, you must state that any views expressed are your own and do not represent Tebbutt Research.

Authority to represent the company social media accounts
- To use Tebbutt Research social media accounts, logos and/or photo/video on behalf of Tebbutt Research you must 
    be an approved user.
- Never use someone else's access password to post as this is a misrepresentation of identity.
- If you don't have your own password, you are not an authorised user. Request access via the Manager or 
    Project Manager.

**Posting social media content**\
- Never publish, post or release negative, sensitive, private or confidential company information 
    (e.g. unannounced events or internal issues) that could damage the organisation's reputation or breach 
    legislative compliance.
- Never post photos of or make negative comments about Tebbutt Research or employees and other stakeholders 
    and do not share negative comments made by other persons.

**Privacy**\
Respect Tebbutt Research privacy obligations under the jurisdictional laws.

Never give out personal information about co-workers (e.g. private and personal comments) or add personal 
information you see on social networking to other network sites. This could be construed as harassing, threatening, 
retaliatory or discriminatory.

Ensure your posts do not create a real or perceived conflict of interest with Tebbutt Research. This applies to 
accepting gratuities, making comments or advertising in conflict with Tebbutt Research.

**If you think this policy may have been breached**\
If you notice inappropriate or unlawful content posted online relating to Tebbutt Research notify the General 
Manager immediately providing as much information as you can about where/who/what has been posted.

If you inadvertently post a comment or make references that you believe in retrospect were inappropriate or 
unintended for the user audience, or you think you may have breached privacy legislation then:

- Attempt to remove or delete the post immediately, or
- Contact the social networking site and request it be removed, then
- Contact a member of the senior management team (Managing Director, Manager) as soon as possible indicating 
    what has happened. By notifying the company whilst you still retain some liability your honesty and quick 
    response is taken into account should any further action be necessary.