Title: Privacy and Information Security Handbook Section 11
Authors: Tim Wilson
Created: 2022-10-05
Edited: 2022-11-21
Classification: <a href="InfoSecHandbook03">Confidential</a>

# Privacy and Information Security Handbook
[<< Section 10](InfoSecHandbook10.md) | [Index](InfoSecHandbook) | [Section 12>>](InfoSecHandbook12.md)

## 11.0 Cryptography policy

> **Objective**\
> The objective of using cryptography is to protect the confidentiality, integrity and availability of the 
> organisation's secure electronic data.

The vast majority of information that leaves Tebbutt Research is not sensitive or personal identifying information. 
Care is taken to ensure data is de-identified. In circumstances where personal identifying information needs to be 
transferred, such as client sample data, methods such as password protected compressed/encrypted files such as 
SharePoint through a password protected file (typically Excel) which is password protected and then the password 
is shared either though Text SMS or embedded in a Word doc (password protected that has link and password) in a 
separate email.

The method used is determined by the owner of the data as they are sending it to us. Additionally, we use secure 
uploading facilities with SSL whenever possible (either our own portal, or a client's Dropbox for example).

Respondent interactions with our servers use HTTPS (SSL) connections ensuring data communications between a 
respondent's device and our servers (or our survey platform) are encrypted. Data collected on our survey 
platform (Nfield) is encrypted in transit and at rest.

A risk assessment determines the need and level of cryptographic controls taking into consideration confidentially, 
integrity and authenticity of information security needs.