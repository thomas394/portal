Title: Privacy and Information Security Handbook Section 16
Authors: Tim Wilson
Created: 2022-10-05
Edited: 2022-11-21
Classification: <a href="InfoSecHandbook03">Confidential</a>

# Privacy and Information Security Handbook
[<< Section 15](InfoSecHandbook15.md) | [Index](InfoSecHandbook) | [Section 17>>](InfoSecHandbook17.md)

## 16.0 Third party/supplier, subcontractor management policy
> **Objective**\
> Risk mitigation in supplier relationships are managed and monitored to ensure aspects of confidentiality, 
> integrity and availability of data and systems is never compromised.


Tebbutt Research enters into supplier relationships with third parties for the purposes of information 
technology service provision including:
- malware/virus protections,
- computer hardware,
- computer software,
- outsourced software development,
- quality assurance testing,
- data storage, and
- infrastructure

Other third-party supplier relationships include:
- Legal / contractual advice
- Survey tools (Nfield, Dapresy, Recollective) – end user licence

Whilst outsourcing involves transferring the assignment for carrying out an activity that is normally 
undertaken in house, this does not negate Tebbutt Research' ownership or accountability for any outcomes 
arising from the outsourced activity.

Tebbutt Research assures services provided by third parties meet business requirements through selection, 
monitoring and management processes. This includes vendor selection process and by defining the roles, 
responsibilities and expectations in third-party agreements as well as reviewing and monitoring such 
agreements for effectiveness and compliance.

Tebbutt Research does not allow any agreement to be entered providing third party access that is risk 
assessed to have a potential information security risk without executive approval to accept that risk.