Title: Privacy and Information Security Handbook Section 10
Authors: Tim Wilson
Created: 2022-10-05
Edited: 2022-11-21
Classification: <a href="InfoSecHandbook03">Confidential</a>

# Privacy and Information Security Handbook
[<< Section 9](InfoSecHandbook09.md) | [Index](InfoSecHandbook) | [Section 11>>](InfoSecHandbook11.md)

## 10.0 Access control policy
> **Objective**\
> Tebbutt Research shall limit access to information and information processing facilities to prevent 
> unauthorised access and make users accountable for safeguarding their authentication.

Access control to Tebbutt Research software applications and data is based on:

- Security requirements of Tebbutt Research applications and systems
- Information classification of the individual user and their role within Tebbutt Research or externally
- User '_need to know and need to use_' principle
- Legislative implications including jurisdictional issues
- Network access rights across the whole of the Tebbutt Research network
- Segregation needs - authorisations and access administration to ensure data security and integrity is maintained
- Special one off or periodic needs - includes compliance reviews/audits
- Privileged access rights provided for scheduled works and development
- Removal of access rights when no longer required or in the case of suspected misuse

Above all else, access is assessed on the basis of "forbidden unless expressly permitted".

On appointment and signing an employment agreement, access controls will be set and tested by the person issuing 
access control.

When roles and responsibilities change, access controls may also vary. This is under control of the 
Managing Director and General Manager who will determine and allow any variation to scope and access levels.

- SharePoint access – is for active projects only
- Login with username and password and then code through the Duo authenticator app.

**The basic rules when assigning a user ID**\
- A User ID shall only be assigned to persons whose role requires access, confirmed by the General Manager 
    before requesting access.
- The person assigned the User ID is responsible for all tasks carried out under the User ID
- User IDs will be set to expire at the end of contract date or after a specified period of time
- Exceptions will only be considered when there is a clear business case, the request is submitted for 
    approval in writing (email) and approved by the General Manager and Managing Director.
- Before receiving the User ID, as a condition of access, the user must be made aware of the company privacy 
    and information security policies.

**Password protection - User access management**\
User access management process requires user registration as a minimum:

- SharePoint and Local Area Network passwords are reset every 3 months.
- MFA (Duo) is required for access outside the local network.
- Deregistration occurs at termination, role change as appropriate, or when needed determined by Managing Director 
    or General Manager.

**Privileged access rights**\
Privileged access rights are generally admin accounts used by the Information Communications Technology (ICT) 
representative from Evolve Technologies to perform maintenance on workstations, servers, network devices, 
websites and databases. These are restricted to roles with specific needs, for a limited time and only as 
authorized by the senior management team.

**Secret authentication information of users**\
This is considered necessary for any key holders including encryption keys or source code access.

Access is kept confidential to the user, approved by senior management team (Managing Director and General Manager) 
and issued by the Managing Director.

Secret authentication passwords are securely retained and reviewed 6 monthly to ensure it remains valid.

**Password Protection - User responsibilities**\
Users are held accountable for the security of their access and are required to ensure their authentication 
is safeguarded.

**Password minimum standards – permanent staff**\
All permanent staff must use LastPass for password generation and safekeeping. Multi-factor authentication (MFA) 
must be enabled on LastPass. The master password must be a passphrase of at least 20 characters. 
LastPass must be set to generate secure passwords of length 12 using upper-case, lower-case, numbers, and symbols.

**Password minimum standards – casual staff**\
Casual staff must use complex / strong passwords for all company services. This is defined as a password 
that is reasonably difficult to guess in a short period of time either through human guessing or the use of 
specialized software. As a minimum a password shall include:

- 8 characters as a minimum - better to be closer to 12
- Mix of upper/lower case character
- At least one numerical character

Don't base passwords on personal information alone.

Passphrases are better than a password. Whilst not mandatory, they are encouraged. A good passphrase 
should have at least 15, preferably 20 characters and be difficult to guess. It should contain upper case letters, 
lower case letters, digits, and preferably at least one punctuation character. No part of it should be derivable 
from personal information about the user or his/her family.