Title: Privacy and Information Security Handbook Section 4
Authors: Tim Wilson
Created: 2022-10-05
Edited: 2022-11-21
Classification: <a href="InfoSecHandbook03">Confidential</a>

# Privacy and Information Security Handbook
[<< Section 3](InfoSecHandbook03.md) | [Index](InfoSecHandbook) | [Section 5>>](InfoSecHandbook05.md)

# 4.0 Risk Management Framework

## Tebbutt Research Risk Management Framework

>**Mandate and Commitment:**\
>The Managing Director and General Managers have recognised that the effective management of risk is central to the achievement of Tebbutt Research's strategic goals.

### The risk management process

**Identify risks**\
Identification of possible risks using a structured and systematic process is critical. Identification should include all risks, whether or not under the control of Tebbutt Research.

**Analyse risks**\
Risk analysis provides an input to decisions on whether risks need to be treated and the most appropriate and cost-effective risk treatment strategies.

Factors that affect consequences and likelihood may be identified. Risk is analysed by combining consequences and their likelihood. In most circumstances existing controls are taken into account.

**Evaluate risks**\
Risk evaluation involves comparing the level of risk found during the analysis process with risk criteria established when the context was considered.

**Treat risks**\
Risk treatment involves identifying the range of options for treating risks Tebbutt Research will undertake activities that have significant risks associated with them.

**Monitor and review**\
On-going review is essential to ensure that the risks remain relevant. Factors that may affect the likelihood and consequences of an outcome may change, as may the factors that affect the suitability or cost of the treatment options.

### Information Security Risk Framework

![img.png](/static/content/risk01.png)

![img_1.png](/static/content/risk02.png)

![img_2.png](/static/content/risk03.png)

### Risk Management Responsibilities

| **Role** | **Responsibilities**                                                                                                                                                                                                                                                                                  |
| --- |-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Managing Director | - Retains the ultimate responsibility for risk management and for determining the appropriate level of risk that Tebbutt Research is willing to accept.                                                                                                                                               |
| General Managers | - Overseeing the risk management activities at Tebbutt Research.<br>- Providing advice on appropriate risk management related procedures and measurement methodologies throughout Tebbutt Research.<br>- Ensures that risk management activities are carried out effectively within Tebbutt Research. |
| Staff | - Implementation of process changes and/or risk treatment strategies.<br>- Day-to-day operation of controls.                                                                                                                                                                                          |
