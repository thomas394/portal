Title: Privacy and Information Security Handbook Section 9
Authors: Tim Wilson
Created: 2022-10-05
Edited: 2022-11-21
Classification: <a href="InfoSecHandbook03">Confidential</a>

# Privacy and Information Security Handbook
[<< Section 8](InfoSecHandbook08.md) | [Index](InfoSecHandbook) | [Section 10>>](InfoSecHandbook10.md)

## 9.0 Asset management policy
> **Objective**\
> Tebbutt Research assets are considered the assets retained in the Gold Coast office.

Asset management covers organisational assets and defines appropriate protection requirements and media handling. 
Assets are managed throughout their lifecycle from procurement to redundancy including processing, storage, 
transmission, deletion, and destruction.

Asset information is collected and stored in the Tebbutt Research Asset register.