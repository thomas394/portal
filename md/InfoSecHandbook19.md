Title: Privacy and Information Security Handbook Section 19
Authors: Tim Wilson
Created: 2022-10-05
Edited: 2022-11-21
Classification: <a href="InfoSecHandbook03">Confidential</a>

# Privacy and Information Security Handbook
[<< Section 18](InfoSecHandbook18.md) | [Index](InfoSecHandbook) | [Appendix A>>](InfoSecHandbookA.md)

## 19.0 Compliance management policy
>**Objective**\
> Tebbutt Research ensures identification and management of breaches of legal, statutory, regulatory 
> or contractual obligations are considered to ensure operations in accordance with policies and procedures are met.

Tebbutt Research information compliance management on the basis of:
- Audit, inspection and testing of risk to confidentiality, integrity and accessibility in relation to 
    Tebbutt Research systems - privacy and protection of personally identifiable information
- Compliance to legal and regulatory requirements
- Compliance to Tebbutt Research information security operational policies and procedures
- Intellectual property rights in regards to legal use of the software

## 19.1 Nonconformity, corrective action and improvement policy
>**Objective**\
> Tebbutt Research ensures identification and management incidents, breaches, corrective action and 
> improvements are managed to ensure legal compliance and continuous improvement.

All non-conformity will be reviewed, and corrective actions taken. Improvements to processes will be sought.