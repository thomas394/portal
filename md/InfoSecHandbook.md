Title: Privacy and Information Security Handbook Section 1
Authors: Tim Wilson
Created: 2022-10-05
Edited: 2022-11-21
Classification: <a href="InfoSecHandbook03">Confidential</a>

# Privacy and Information Security Handbook

## Policies and Objectives Guidance and Awareness Handbook

### Version History

| Revision   | Changes Made          | Reviewed and Approved        | Training Required? |
|------------|-----------------------|------------------------------|--------------------|
| 2022-10-05 | Initial document      | Tim Wilson                   | Yes |
| 2022-11-07 | Review & update       | Draft for comment and review | |
| 2022-11-21 | Create online version | Tim Wilson | | 

## Contents
1. [Confidentiality and Privacy Statement](InfoSecHandbook01)
2. [Legal and Industry Considerations](InfoSecHandbook02)
3. [Information Classification Policy](InfoSecHandbook03)
4. [Risk Management Framework](InfoSecHandbook04)
5. [Data Breach Response Plan](InfoSecHandbook05)
6. [Information Security Policy](InfoSecHandbook06)
7. [Organisation of information security policy](InfoSecHandbook07)
8. [Human resources security policy](InfoSecHandbook08)
9. [Asset management policy](InfoSecHandbook09)
10. [Access control policy](InfoSecHandbook10)
11. [Cryptography policy](InfoSecHandbook11)
12. [Physical and environmental policy](InfoSecHandbook12)
13. [Operations security policy](InfoSecHandbook13)
14. [Communications policy](InfoSecHandbook14)
15. [Systems acquisition, development, and maintenance policy](InfoSecHandbook15)
16. [Third party/supplier, subcontractor management policy](InfoSecHandbook16)
17. [Incident security management policy](InfoSecHandbook17)
18. [Information security aspects of business continuity policy](InfoSecHandbook18)
19. [Compliance management policy](InfoSecHandbook19)

* [Appendix A – Data and Privacy Legislation across Jurisdictions](InfoSecHandbookA)
* [Appendix B – Australian Privacy Principles](InfoSecHandbookB)
