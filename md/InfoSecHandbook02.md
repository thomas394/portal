Title: Privacy and Information Security Handbook Section 2
Authors: Tim Wilson
Created: 2022-10-05
Edited: 2022-11-21
Classification: <a href="InfoSecHandbook03">Confidential</a>

# Privacy and Information Security Handbook
[<< Section 1](InfoSecHandbook01.md) | [Index](InfoSecHandbook) | [Section 3>>](InfoSecHandbook03.md)

## 2.0 - Legal and Industry Considerations

### Legal considerations – Australian jurisdiction

The Privacy Commissioner has power under the Privacy Act to conduct investigations, ensure compliance with the Privacy Act and seek civil penalties for a serious/egregious breach or for repeated breaches of the APPs where remediation has not been implemented. This particularly applies to issues relating to information classification and subsequent breaches

Other State and Federal legislation that impact on or relate to data protection and impact privacy/data protection for specific types of data or for specific activities include:\
* Telecommunications Act 1997 (Cth)
* National Health Act 1953 (Cth)
* Health Records and Information Privacy Act 2002 (NSW)
* Health Records Act 2001 (Vic)
* Workplace Surveillance Act 2005 (NSW)

### European Union (EU) data considerations – transfer of data to/from the EU
The General Data Protection Regulation (Regulation (EU) 2016/679) (GDPR) is a European Union law which entered into force in 2016 and became directly applicable law in all Member States of the European Union on 25 May 2018.

The GDPR has extra-territorial effect. An organisation that it is not established within the EU will still be subject to the GDPR if it processes personal data of data subjects who are in the Union where the processing activities are related "to the offering of goods or services" (Article 3(2)(a)) (no payment is required) to such data subjects in the EU or "the monitoring of their behaviour" (Article 3(2)(b)) as far as their behaviour takes place within the EU.

### Industry considerations – Australia jurisdiction
* The Research Society [Code of Professional Behaviour](https://researchsociety.com.au/standards/code-of-professional-behaviour)
* The Research Society fact sheet - [MSR and the Privacy Act 1988 and Australian Privacy Principles](https://researchsociety.com.au/documents/item/1541)
* ISO 20252: 2019 Clause 4.3 Information Security

### Industry considerations – International jurisdiction
* The [ESOMAR Code of Conduct](https://esomar.org/code-and-guidelines/icc-esomar-code)
* General Data Protection Regulation (GDPR) policy
* Various legislation across the countries we work in – see [Appendix A](InfoSecHandbookA.md)

### Authorities and responsibilities for Data Privacy at the Tebbutt Research
Responsibilities and authorisations are assigned and included in position descriptions. Information classification is also explained at induction and awareness (ongoing) via programmed training.

###Internal Stakeholders###
| **Title**                                                                                                               | **Role**                                                                                                                                                                                                                                                                                                                                 |
|-------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Tebbutt Research employees                                                                                              | - Report privacy incidents, enquiries and requests to the Data Privacy Officer (DPO)<br>- Document requests, enquiries, incidents as required.                                                                                                                                                                                           
| Data Privacy Officer (DPO)                                                                                              | - Manages all requests and enquiries<br>- Investigates incidents and breaches<br>- contain breach, remediate harm and preserve evidence<br>- Notifies Managing Director on all data breaches immediately<br>- Sits on Data Breach Response Team<br>- Conducts privacy training with employees and members                                |
| Managing Director                                                                                                       | - Receives information on any incidents<br>- Contains breach, remediate harm and preserve evidence<br>- Conducts investigations and completes data breach reports as required<br>- Reports internally and externally to stakeholders including regulators (OAIC)                                                                         |
| Data Breach Response Team<br>Managing Director<br>General Manager<br>IT consultant<br>Programmer/s for relevant systems | - Review investigation findings<br>- Accept or reject findings<br>- Determine seriousness of the data breach<br>- Assess containment and/or remediation actions<br>- Assess preliminary investigation<br>- Assess whether an eligible data breach has occurred<br>- Assess notification requirements<br>- Assist with post-action review |
