Title: Privacy and Information Security Handbook Section 14
Authors: Tim Wilson
Created: 2022-10-05
Edited: 2022-11-21
Classification: <a href="InfoSecHandbook03">Confidential</a>

# Privacy and Information Security Handbook
[<< Section 13](InfoSecHandbook13.md) | [Index](InfoSecHandbook) | [Section 15>>](InfoSecHandbook15.md)

## 14.0 Communications policy
> **Objective**
> Tebbutt Research ensures the protection of information networks, supporting information processes facilities, 
> the security of information transferred within an organization and with any external party including clients.

Tebbutt Research communications security includes network security and information transfer based on consistent 
management and monitoring across the infrastructure including:

- Network access and restrictions in place to ensure information in networks and the connected services are 
    protected from unauthorised access
- Assigned responsibilities for network operations and computer operations are clearly defined and separated
- Protection of confidentiality and integrity of data passing over public networks through safeguarding measures
- Access authentication across the network to restrict access to planned arrangements