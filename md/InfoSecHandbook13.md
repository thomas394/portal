Title: Privacy and Information Security Handbook Section 13
Authors: Tim Wilson
Created: 2022-10-05
Edited: 2022-11-21
Classification: <a href="InfoSecHandbook03">Confidential</a>

# Privacy and Information Security Handbook
[<< Section 12](InfoSecHandbook12.md) | [Index](InfoSecHandbook) | [Section 14>>](InfoSecHandbook14.md)

## 13.0 Operations security policy
> **Objective**\
> To ensure correct, secure operations of information processing facilities including:
> - Protection against malware
> - Backups to protect the loss of data
> - System controls to ensure integrity is maintained
> - Technical vulnerabilities are managed
> - Audits/tests are managed without interruption to systems and services

Operations security protects and secures the sustainability of operations, protection, backup, monitoring, 
control, vulnerability management and performance monitoring to ensure information confidentiality and integrity.

Back up procedures are as follows:

- MS Exchange; Tebbutt Research maintain a daily backup copy on the server itself, as well as incremental 
  off-site snapshots. In the event of complete incapacitation of the server, we could restore data from previous 
  hours (up to a day) / previous days (up to a week) / previous weeks (up to a month) and previous months 
  (up to a year).
- SharePoint data is managed by MS datacentres and each data centre maintains at least one shadow copy at an 
  alternative physical location. Built into SharePoint is the ability to roll back to multiple earlier versions 
  of any documents as well as to retrieve files from a recycle bin which may have been inadvertently deleted.
- The Tebbutt Research website is managed by Evolve Technologies and backed up to their own servers on an 
  incremental schedule (days / weeks / months).