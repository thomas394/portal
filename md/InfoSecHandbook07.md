Title: Privacy and Information Security Handbook Section 7
Authors: Tim Wilson
Created: 2022-10-05
Edited: 2022-11-21
Classification: <a href="InfoSecHandbook03">Confidential</a>

# Privacy and Information Security Handbook
[<< Section 6](InfoSecHandbook06.md) | [Index](InfoSecHandbook) | [Section 8>>](InfoSecHandbook08.md)

## 7.0 Organisation of information security policy
> **Objective**\
> Tebbutt Research will manage business processes, information compliance and regulatory compliance using MS Office 
> productivity tools for implementing systems and retaining pertinent records.

The Tebbutt Research Information Security Management System (ISMS) framework for the management of information 
security is document version controlled and stored on the Tebbutt Research internal network share drive.

Gold Coast, Australia is the head office of operations, and the registered business address.

Data collected through online fieldwork, or provided by clients, is stored online on a secure Microsoft SharePoint 
server. On the SharePoint server there are multiple levels of access: Administration access rights (all records), 
Senior management access rights (past and current projects), Finance manager access rights (HR and accounting records), 
Project manager access rights (current projects), Operations and Quality Assurance permanent employee access rights 
(operations records), and casual staff access rights (restricted to specific folders only).

Tebbutt Research uses Microsoft Office 365 with all data located in Australian data centres meeting Australian 
data residency requirements. We carefully and centrally manage our editing and access permissions to ensure data 
is secure. Office 365 uses service-side technologies that encrypt customer data at rest and in transit. 
For customer data at rest, Office 365 uses volume-level and file-level encryption. For customer data in transit, 
Office 365 uses multiple encryption technologies for communications between data centres and between clients and 
servers, such as Transport Layer Security (TLS) and Internet Protocol Security (IPsec). Office 365 also includes 
customer-managed encryption features.

Microsoft is also included on the Australian Certified Cloud Services List for both Unclassified: Dissemination 
Limiting Markers (DLM) and PROTECTED data based on an IRAP (Information Security Registered Assessors Program) 
assessment and certification by the Australian Cyber Security Centre (ACSC). Microsoft has appropriate and effective 
security controls in place for the processing, storage, and transmission of DLM and PROTECTED classified information.

## 7.1 Remote and teleworking policy
> **Objective**\
> Remote and teleworking is a process at Tebbutt Research with protocols in place to ensure information security 
> of client or Tebbutt Research data is never compromised.

Tebbutt Research considers remote and teleworking working to be any form of work outside the local office. 
This includes working from home and telecommuting when travelling including hotels and in transit locations.

Access to the Gold Coast office network is as follows:

- Confidential information related to project implementation may be accessed through the Tebbutt Research cloud 
- functions with data transferred via secure SharePoint or equivalent secure alternatives as agreed with the client.
- The Tebbutt Research SharePoint site is accessed through 2 factor authentication using Duo.

Technologies including desktop computers, laptops and mobile phones are password protected. Data is not held on 
any remote device unless hardware encrypted.

All Tebbutt Research employees (both full time and casual) sign a work from home agreement that includes 
confidentiality and IP protections and states that they have agreed to do the following:

- Not leave their computer unlocked and unattended;
- Data is not saved to desktop;
- They must work in the cloud using the company SharePoint

Portable devices are not to be left unattended in public places and are subject to a timeout screen saver 
requiring a login access.

